# Ascension from Beneath the Abyss

### Summary

A LÖVE game for the BaconGameJam 09 themed "depth".

![screenshot](scrot.png)

### Controls

Arrow keys, WASD or D-pad to move; space or start button to pause.
